﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Notification;
using Estat.Sdmxsource.Extension.Builder;
using Estat.Sdmxsource.Extension.Model.Error;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using RabbitMQ.Client;

namespace DotStat.Producer
{
    public class RabbitMqProducer : IStructureChangesObserver
    {
        private readonly IConfiguration _configuration;

        public RabbitMqProducer(IConfiguration configuration)
        {
            _configuration = configuration; 
        }

        public Task Notify(IList<IResponseWithStatusObject> summary, IPrincipal principal)
        {
            var notificationConf = _configuration.GetSection("rabbitMq").Get<MesssageBrokerConfiguration>();

            if (notificationConf == null || notificationConf.Enabled == false)
                return Task.CompletedTask;

            var factory = new ConnectionFactory()
            {
                HostName = notificationConf.HostName,
                VirtualHost = notificationConf.VirtualHost,
                Port = notificationConf.Port,
                UserName = notificationConf.UserName,
                Password = notificationConf.Password
            };

            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare(
                queue: notificationConf.QueueName,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var queueItem = new QueueItem(
                notificationConf.Dataspace,
                (principal.Identity as ClaimsIdentity)?.FindFirst("name")?.Value, 
                (principal.Identity as ClaimsIdentity)?.FindFirst("email")?.Value,
                summary
                    .Where(y => y.StructureReference.MaintainableStructureEnumType == SdmxStructureEnumType.Dataflow)
                    .Select(x => x.StructureReference.MaintainableReference).Cast<MaintainableRefObjectImpl>());

            var body = Encoding.UTF8.GetBytes(
                JsonConvert.SerializeObject(queueItem)
                );

            channel.BasicPublish(
                exchange: "",
                routingKey: notificationConf.RoutingKey,
                basicProperties: null,
                body: body);

            return Task.CompletedTask;
        }     
    }
}
